require.config({
	baseUrl: '/',
	paths: {
		'jquery': 'public/js/vendor/jquery/dist/jquery',
		'backbone': 'public/js/vendor/backbone/backbone',
		'underscore': 'public/js/vendor/underscore/underscore',
		'bootstrap': 'public/js/vendor/bootstrap/dist/js/bootstrap',
		'dustjs-linkedin': 'public/js/vendor/dustjs-linkedin/dist/dust-full',
		'templates': 'public/js/lib/compiled-amd',
		'MenuItem': 'public/js/MenuItem',
		'MenuItemCollection': 'public/js/MenuItemCollection',
		'MenuItemView': 'public/js/MenuItemView',
		'ItemFormView': 'public/js/ItemFormView',
		'AppView': 'public/js/AppView',
		'utils': 'public/js/utils'
	},
	shim: {
		'bootstrap' : { 'deps': ['jquery'] },
		'dustjs-linkedin': { 'exports': 'dust'}
	}
});

require(['jquery', 'MenuItemCollection', 'AppView', 'bootstrap'], function ($, MenuItemCollection, AppView) {

	$(function() {
		var app = new AppView();
	});
});
