define(['backbone', 'jquery', 'utils', 'MenuItem', 'templates'], function (Backbone, $, utils, MenuItem) {
	var ItemFormView = Backbone.View.extend({
		className: "modal-content",
		template: "add-menu-item",
		events: {
			'click [data-action="save"]': 'handleFormSave',
			'click [data-action="cancel"]': 'handleFormCancel'
		},

		initialize: function () {
			if (!this.model) {
				this.model = new MenuItem();
			}

			this.render();
		},

		render: function () {
			var self = this;
			var dfd = $.Deferred()

			utils.dustRender(this.template, this.model.toJSON()).then(function (html) {
				self.$el.html(html);
				dfd.resolve(self.el);
			});

			return dfd.promise();
		},

		updateModel: function () {
			this.model.set({
				name: this.$el.find("#itemName").val(),
				description: this.$el.find("#itemDescription").val(),
				price: this.$el.find("#itemPrice").val()
			});
		},

		clearForm: function () {
			this.$el.find("input").val("");
		},

		handleFormSave: function (e) {
			this.updateModel();
			this.clearForm();
			this.trigger("formSave", this.model);
			return false;
		},

		handleFormCancel: function (e) {
			this.clearForm();
			this.trigger("formCancel");
			return false;
		}
	});

	return ItemFormView;
});
