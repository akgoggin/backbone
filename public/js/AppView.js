define(['backbone', 'jquery', 'utils', 'MenuItemView', 'MenuItemCollection', 'ItemFormView', 'MenuItem', 'templates'], function (Backbone, $, utils, MenuItemView, MenuItemCollection, ItemFormView, MenuItem) {
	var AppView = Backbone.View.extend({
		el: $("#app"),
		$formModal: $("#formModal"),

		events: {
			'click [data-action="addNew"]': 'createItem'
		},

		initialize: function () {
			var self = this;
			if (!this.collection) {
				this.collection = new MenuItemCollection();
			}

			this.collection.fetch().then(function () {
				self.render()
			});
		},

		createItem: function () {
			var newItem = new MenuItem();
			this.openForm(newItem);
		},

		openForm: function (item) {
			var self = this;
			this.formView = new ItemFormView();
			this.listenTo(this.formView, "formSave", this.saveForm);
			this.listenTo(this.formView, "formCancel", this.closeForm);

			self.formView.model = item;

			self.formView.render().then(function (html) {
				self.$formModal.find(".modal-dialog").html(html);
				self.$formModal.modal('show');
			});
		},

		saveForm: function (item) {
			if (item.isNew()) {
				this.addMenuItem(item);
			}
			item.save();
			this.closeForm();
		},

		closeForm: function () {
			this.$formModal.modal('hide');
			this.formView.remove();
		},

		addMenuItem: function (item) {
			this.collection.push(item);
			this.render();
		},

		render: function () {
			var self = this;
			self.$el.find("#menu-items").html("");
			_.each(this.collection.models, function (item) {
				var itemView = new MenuItemView ({model: item});
				self.listenTo(itemView, "itemEdit", self.openForm);
				itemView.render().then(function (html) {
					self.$el.find("#menu-items").append(html);
				});
			});
		}
	});

	return AppView;
});
