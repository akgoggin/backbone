define(["dustjs-linkedin"], function(dust) {
  // ./views/add-menu-item.dust
  (function() {
    dust.register("add-menu-item", body_0);

    function body_0(chk, ctx) {
      return chk.write("<div class=\"modal-body\"><form><div class=\"form-group\"><label for=\"itemName\">Name:</label><input type=\"text\" class=\"form-control\" id=\"itemName\" value=\"").reference(ctx._get(false, ["name"]), ctx, "h").write("\"></div><div class=\"form-group\"><label for=\"itemDescription\">Description:</label><input type=\"text\" class=\"form-control\" id=\"itemDescription\" value=\"").reference(ctx._get(false, ["description"]), ctx, "h").write("\"></div><div class=\"form-group\"><label for=\"itemPrice\">Price:</label><input type=\"text\" class=\"form-control\" id=\"itemPrice\" value=\"").reference(ctx._get(false, ["price"]), ctx, "h").write("\"></div></form></div><div class=\"modal-footer\"><button class=\"btn btn-primary\" data-action=\"save\">Save</button><button class=\"btn btn-default\" data-action=\"cancel\">Cancel</button></div>");
    }
    return body_0;
  })();
  // ./views/menu-item.dust
  (function() {
    dust.register("menu-item", body_0);

    function body_0(chk, ctx) {
      return chk.write("<div class=\"col-md-2\">Name: ").reference(ctx._get(false, ["name"]), ctx, "h").write("</div><div class=\"col-md-6\">Description: ").reference(ctx._get(false, ["description"]), ctx, "h").write("</div><div class=\"col-md-2\">Price: ").reference(ctx._get(false, ["price"]), ctx, "h").write("</div><div class=\"col-md-2\"><button class=\"btn btn-default\" data-action=\"edit\">Edit</button><button class=\"btn btn-danger\" data-action=\"delete\">Delete</button></div>");
    }
    return body_0;
  })();
  define("add-menu-item", function() {
    return function(locals, callback) {
      var rendered;

      dust.render("add-menu-item", locals, function(err, result) {
        if (typeof callback === "function") {
          try {
            callback(err, result);
          } catch (e) {}
        }

        if (err) {
          throw err
        } else {
          rendered = result;
        }
      });

      return rendered;
    }
  });
  define("menu-item", function() {
    return function(locals, callback) {
      var rendered;

      dust.render("menu-item", locals, function(err, result) {
        if (typeof callback === "function") {
          try {
            callback(err, result);
          } catch (e) {}
        }

        if (err) {
          throw err
        } else {
          rendered = result;
        }
      });

      return rendered;
    }
  });
  return ["add-menu-item", "menu-item"];
});