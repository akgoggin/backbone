define(['backbone'], function (Backbone) {
	var MenuItem = Backbone.Model.extend({
		idAttribute: "_id",
		ulrRoot: "http://tiy-fee-rest.herokuapp.com/collections/kgoggin"
	});

	return MenuItem;
});
